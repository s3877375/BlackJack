//
//  BlackJackApp.swift
//  BlackJack
//
//  Created by Hieu Le Pham Ngoc on 05/08/2022.
//

import SwiftUI

@main
struct BlackJackApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
