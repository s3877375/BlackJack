//
//  Hello.swift
//  BlackJack
//
//  Created by Hieu Le Pham Ngoc on 09/08/2022.
//

import SwiftUI

struct Hello: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct Hello_Previews: PreviewProvider {
    static var previews: some View {
        Hello()
    }
}
